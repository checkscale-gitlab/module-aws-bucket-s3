#####
# S3 bucket
#####

output "id" {
  value = aws_s3_bucket.this.id
}

output "arn" {
  value = aws_s3_bucket.this.arn
}

output "bucket_domain_name" {
  value = aws_s3_bucket.this.bucket_domain_name
}

output "bucket_regional_domain_name" {
  value = aws_s3_bucket.this.bucket_regional_domain_name
}

output "hosted_zone_id" {
  value = aws_s3_bucket.this.hosted_zone_id
}

output "region" {
  value = aws_s3_bucket.this.region
}

#####
# KMS key
#####

output "kms_key_arn" {
  value = element(concat(aws_kms_key.this.*.arn, [null]), 0)
}

output "kms_key_id" {
  value = element(concat(aws_kms_key.this.*.key_id, [null]), 0)
}

output "kms_alias_arn" {
  value = element(concat(aws_kms_alias.this.*.arn, [null]), 0)
}

output "kms_alias_target_key_arn" {
  value = element(concat(aws_kms_alias.this.*.target_key_arn, [null]), 0)
}

#####
# Service IAM User
#####

output "iam_user_arn" {
  value = element(concat(aws_iam_user.data.*.arn, [null]), 0)
}

output "iam_user_name" {
  value = element(concat(aws_iam_user.data.*.name, [null]), 0)
}

output "iam_user_unique_id" {
  value = element(concat(aws_iam_user.data.*.unique_id, [null]), 0)
}

output "iam_user_iam_access_key_id" {
  value     = element(concat(aws_iam_access_key.data.*.id, [null]), 0)
  sensitive = true
}

output "iam_user_iam_access_key_secret" {
  value     = element(concat(aws_iam_access_key.data.*.secret, [null]), 0)
  sensitive = true
}

#####
# IAM policy
#####

output "iam_policy_read_only_id" {
  value = element(concat(aws_iam_policy.this_read.*.id, [null]), 0)
}

output "iam_policy_read_only_arn" {
  value = element(concat(aws_iam_policy.this_read.*.arn, [null]), 0)
}

output "iam_policy_read_only_description" {
  value = element(concat(aws_iam_policy.this_read.*.description, [null]), 0)
}

output "iam_policy_read_only_name" {
  value = element(concat(aws_iam_policy.this_read.*.name, [null]), 0)
}

output "iam_policy_read_only_json" {
  value = element(concat(data.aws_iam_policy_document.this_read.*.json, [null]), 0)
}

output "iam_policy_full_id" {
  value = element(concat(aws_iam_policy.this_full.*.id, [null]), 0)
}

output "iam_policy_full_arn" {
  value = element(concat(aws_iam_policy.this_full.*.arn, [null]), 0)
}

output "iam_policy_full_description" {
  value = element(concat(aws_iam_policy.this_full.*.description, [null]), 0)
}

output "iam_policy_full_name" {
  value = element(concat(aws_iam_policy.this_full.*.name, [null]), 0)
}

output "iam_policy_full_json" {
  value = element(concat(data.aws_iam_policy_document.this_full.*.json, [null]), 0)
}

output "iam_policy_data_ro_id" {
  value = element(concat(aws_iam_policy.this_objects_ro.*.id, [null]), 0)
}

output "iam_policy_data_ro_arn" {
  value = element(concat(aws_iam_policy.this_objects_ro.*.arn, [null]), 0)
}

output "iam_policy_data_ro_description" {
  value = element(concat(aws_iam_policy.this_objects_ro.*.description, [null]), 0)
}

output "iam_policy_data_ro_name" {
  value = element(concat(aws_iam_policy.this_objects_ro.*.name, [null]), 0)
}

output "iam_policy_data_ro_json" {
  value = element(concat(data.aws_iam_policy_document.this_data_ro.*.json, [null]), 0)
}

output "iam_policy_data_rw_id" {
  value = element(concat(aws_iam_policy.this_objects_rw.*.id, [null]), 0)
}

output "iam_policy_data_rw_arn" {
  value = element(concat(aws_iam_policy.this_objects_rw.*.arn, [null]), 0)
}

output "iam_policy_data_rw_description" {
  value = element(concat(aws_iam_policy.this_objects_rw.*.description, [null]), 0)
}

output "iam_policy_data_rw_name" {
  value = element(concat(aws_iam_policy.this_objects_rw.*.name, [null]), 0)
}

output "iam_policy_data_rw_json" {
  value = element(concat(data.aws_iam_policy_document.this_data_rw.*.json, [null]), 0)
}

output "iam_policy_kms_ro_json" {
  value = element(concat(data.aws_iam_policy_document.kms_read.*.json, [null]), 0)
}

output "iam_policy_kms_data_ro_json" {
  value = element(concat(data.aws_iam_policy_document.kms_read_data.*.json, [null]), 0)
}

output "iam_policy_kms_rw_json" {
  value = element(concat(data.aws_iam_policy_document.kms_full.*.json, [null]), 0)
}

output "iam_policy_kms_data_rw_json" {
  value = element(concat(data.aws_iam_policy_document.kms_write_data.*.json, [null]), 0)
}
