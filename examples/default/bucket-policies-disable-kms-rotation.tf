module "policy_no_kms_rotation" {
  source = "../../"

  name                  = "tftestbpnr${random_string.this.result}"
  bucket_policy_enabled = true
  bucket_policy_json    = data.aws_iam_policy_document.policy_no_rotation.json

  kms_key_create           = true
  kms_key_name             = "tftestbpnr${random_string.this.result}"
  kms_key_alias_name       = "tftestbpnr${random_string.this.result}"
  kms_key_rotation_enabled = false
  kms_key_policy_json      = data.aws_iam_policy_document.kms.json
  iam_policy_create        = true
  iam_policy_read_name     = "tftestbpreadnr${random_string.this.result}"
  iam_policy_full_name     = "tftestbpfullnr${random_string.this.result}"
  iam_policy_data_ro_name  = "tftestbpronr${random_string.this.result}"
  iam_policy_data_rw_name  = "tftestbprwnr${random_string.this.result}"

  tags = {
    testTag = "tftest"
  }
}
