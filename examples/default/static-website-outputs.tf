#####
# S3 bucket
#####

output "static_website_id" {
  value = module.static_website.id
}

output "static_website_arn" {
  value = module.static_website.arn
}

output "static_website_bucket_domain_name" {
  value = module.static_website.bucket_domain_name
}

output "static_website_bucket_regional_domain_name" {
  value = module.static_website.bucket_regional_domain_name
}

output "static_website_hosted_zone_id" {
  value = module.static_website.hosted_zone_id
}

output "static_website_region" {
  value = module.static_website.region
}

#####
# KMS key
#####

output "static_website_kms_key_arn" {
  value = module.static_website.kms_key_arn
}

output "static_website_kms_key_id" {
  value = module.static_website.kms_key_id
}

output "static_website_kms_alias_arn" {
  value = module.static_website.kms_alias_arn
}

output "static_website_kms_alias_target_key_arn" {
  value = module.static_website.kms_alias_target_key_arn
}

#####
# Service IAM User
#####

output "static_website_iam_user_arn" {
  value = module.static_website.iam_user_arn
}

output "static_website_iam_user_name" {
  value = module.static_website.iam_user_name
}

output "static_website_iam_user_unique_id" {
  value = module.static_website.iam_user_unique_id
}

output "static_website_iam_user_iam_access_key_id" {
  value     = module.static_website.iam_user_iam_access_key_id
  sensitive = true
}

output "static_website_iam_user_iam_access_key_secret" {
  value     = module.static_website.iam_user_iam_access_key_secret
  sensitive = true
}

#####
# IAM policy
#####

output "static_website_iam_policy_read_only_id" {
  value = module.static_website.iam_policy_read_only_id
}

output "static_website_iam_policy_read_only_arn" {
  value = module.static_website.iam_policy_read_only_arn
}

output "static_website_iam_policy_read_only_description" {
  value = module.static_website.iam_policy_read_only_description
}

output "static_website_iam_policy_read_only_name" {
  value = module.static_website.iam_policy_read_only_name
}

output "static_website_iam_policy_read_only_json" {
  value = module.static_website.iam_policy_read_only_json
}

output "static_website_iam_policy_full_id" {
  value = module.static_website.iam_policy_full_id
}

output "static_website_iam_policy_full_arn" {
  value = module.static_website.iam_policy_full_arn
}

output "static_website_iam_policy_full_description" {
  value = module.static_website.iam_policy_full_description
}

output "static_website_iam_policy_full_name" {
  value = module.static_website.iam_policy_full_name
}

output "static_website_iam_policy_full_json" {
  value = module.static_website.iam_policy_full_json
}

output "static_website_iam_policy_data_ro_id" {
  value = module.static_website.iam_policy_data_ro_id
}

output "static_website_iam_policy_data_ro_arn" {
  value = module.static_website.iam_policy_data_ro_arn
}

output "static_website_iam_policy_data_ro_description" {
  value = module.static_website.iam_policy_data_ro_description
}

output "static_website_iam_policy_data_ro_name" {
  value = module.static_website.iam_policy_data_ro_name
}

output "static_website_iam_policy_data_ro_json" {
  value = module.static_website.iam_policy_data_ro_json
}

output "static_website_iam_policy_data_rw_id" {
  value = module.static_website.iam_policy_data_rw_id
}

output "static_website_iam_policy_data_rw_arn" {
  value = module.static_website.iam_policy_data_rw_arn
}

output "static_website_iam_policy_data_rw_description" {
  value = module.static_website.iam_policy_data_rw_description
}

output "static_website_iam_policy_data_rw_name" {
  value = module.static_website.iam_policy_data_rw_name
}

output "static_website_iam_policy_data_rw_json" {
  value = module.static_website.iam_policy_data_rw_json
}

output "static_website_iam_policy_kms_ro_json" {
  value = module.static_website.iam_policy_kms_ro_json
}

output "static_website_iam_policy_kms_data_ro_json" {
  value = module.static_website.iam_policy_kms_data_ro_json
}

output "static_website_iam_policy_kms_rw_json" {
  value = module.static_website.iam_policy_kms_rw_json
}

output "static_website_iam_policy_kms_data_rw_json" {
  value = module.static_website.iam_policy_kms_data_rw_json
}
