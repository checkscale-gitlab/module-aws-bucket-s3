#####
# Object lock with policy example
#####

data "aws_iam_policy_document" "olwp" {
  statement {
    sid = "1"

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::744480654312:root",
      ]
    }

    actions = [
      "s3:*",
    ]
    resources = [
      module.object_lock_with_policy.arn,
    ]
  }
}

module "object_lock_with_policy" {
  source = "../../"

  name                  = "tftestolwp${random_string.this.result}"
  bucket_policy_enabled = true
  bucket_policy_json    = data.aws_iam_policy_document.olwp.json

  object_lock_configuration = {
    rule = {
      default_retention = {
        mode = "COMPLIANCE"
        days = 1
      }
    }
  }

  lifecycle_configuration_rules = [
    {
      id     = "basic"
      status = "Enabled"
      abort_incomplete_multipart_upload = {
        days_after_initiation = 0
      }
      expiration = {
        days                         = 1
        expired_object_delete_marker = false
      }
      filter = {
        and = {
          0 = {
            object_size_greater_than = 1000
            object_size_less_than    = 100000
            tag = {
              key   = "test"
              value = "test"
            }
          }
        }
      }
    }
  ]

  kms_key_create          = true
  kms_key_name            = "tftestolwp${random_string.this.result}"
  kms_key_alias_name      = "tftestolwp${random_string.this.result}"
  iam_policy_create       = true
  iam_policy_read_name    = "tftestolwpread${random_string.this.result}"
  iam_policy_full_name    = "tftesolwpfull${random_string.this.result}"
  iam_policy_data_ro_name = "tftestolwpro${random_string.this.result}"
  iam_policy_data_rw_name = "tftestolwprw${random_string.this.result}"

  tags = {
    testTag = "tftest"
  }
}
